﻿// See https://aka.ms/new-console-template for more information
using Prototype;
Console.WriteLine("Prototype homework\n" +
	"We created list of objects.\n" +
	"Cloned it with two methods^native ICloneable and MyClone\n" +
	"After that we changed one class field info and checked list state\n");
var _setupUnits = new List<SetupUnit>();
VoltageSupply _voltageSupply = new VoltageSupply("init", 35);
_setupUnits.Add(_voltageSupply);
HighVoltageSupply _highVoltageSupply = new HighVoltageSupply("second supply", 1);
_setupUnits.Add(_highVoltageSupply);
var _myClone = CloneMy(_setupUnits);
var _iCloneable = CloneIcloneable(_setupUnits);
Console.WriteLine("first clone:");
ShowAllinfo(_setupUnits, _myClone, _iCloneable);
_setupUnits[0].SetupInfo.Id = 7;
_setupUnits[1].SetupInfo.Id = 44;
Console.WriteLine("after change:");
ShowAllinfo(_setupUnits, _myClone, _iCloneable);
Console.ReadKey();


void ShowAllinfo(List<SetupUnit> _ini, List<SetupUnit> _my, List<SetupUnit> _icl)
{
	Console.WriteLine("initial objects:");
	ShowInfo(_ini);
	Console.WriteLine("MyClone objects:");
	ShowInfo(_my);	
	Console.WriteLine("ICloneable objects:");
	ShowInfo(_icl);
}
void ShowInfo(List<SetupUnit> _list)
{
	foreach (SetupUnit unit in _list)
	{
		Console.WriteLine("Name = {0}, Id = {1}",unit.Name,unit.SetupInfo.Id);
	}
}
List<SetupUnit> CloneMy(List<SetupUnit> _list)
{
	List<SetupUnit> ans = new();
	foreach (SetupUnit _unit in _list)
	{
		ans.Add(_unit.MyClone());	
	}
	return ans;
}
List<SetupUnit> CloneIcloneable(List<SetupUnit> _list)
{
	List<SetupUnit> ans = new();
	foreach (SetupUnit _unit in _list)
	{
		ans.Add((SetupUnit)_unit.Clone());
	}
	return ans;
}
