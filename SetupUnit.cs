﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	/// <summary>
	/// Узел установки
	/// </summary>
	public abstract class SetupUnit : IMyCloneable<SetupUnit>, ICloneable
	{
		public string Name { get; init; }
		public SetupInfo SetupInfo { get; set; }
		public SetupUnit(string name, int id)
		{
			Name = name;
			SetupInfo = new SetupInfo(id);
		}
		public abstract SetupUnit MyClone();
		public abstract object Clone();
	}
	public class SetupInfo
	{
		public int Id { get; set; }
		public SetupInfo(int id)
		{
			Id = id;
		}
	}
}
