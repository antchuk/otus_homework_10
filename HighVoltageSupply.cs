﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	public class HighVoltageSupply : VoltageSupply
	{		
		public HighVoltageSupply(string name, int id) :base(name,id) 
		{
		}
		public override HighVoltageSupply MyClone()
		{
			return new HighVoltageSupply(this.Name, this.SetupInfo.Id);
		}
		public override object Clone()
		{
			return MemberwiseClone();//new HighVoltageSupply(this.Name,this.SetupInfo.Id);
		}
	}
}
