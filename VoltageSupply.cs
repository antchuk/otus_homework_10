﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
	/// <summary>
	/// Источник напряжения
	/// </summary>
	public class VoltageSupply : SetupUnit
	{		
		public VoltageSupply(string name, int id) : base(name, id)
		{
		}
		public override SetupUnit MyClone()
		{
			return  new VoltageSupply(this.Name, this.SetupInfo.Id);
		}
		public override object Clone()
		{
			return MemberwiseClone();//new VoltageSupply(this.Name,this.SetupInfo.Id);
		}
	}
}
